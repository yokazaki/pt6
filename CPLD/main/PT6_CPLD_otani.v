`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: ICEPP
// Engineer: Ikuo Otani
// 
// Create Date:    10/22/2012 
// Design Name: 
// Module Name:    pt6_otani 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision:
// Revision
// Additional Comments:  
//
//////////////////////////////////////////////////////////////////////////////////


module top
(

input		wire				CLK,				// EXT_CLK (LEMO) or VME_CLK, see JP1
input		wire				VME_CLK,			// 40MHz oscillator
input		wire				RESET_B,			// push button on front panel
input		wire				POK1,				// 3.3V power
input		wire				POK3,				// 1.8V power
input		wire				POK4,				// 1.2V power
output	wire	[4:0]		LED,				// {FPGA_IA_OUT, FPGA_STATE}
output	wire	[15:0]	CPLD_TEST,		// testpin for CPLD
output	wire	[1:0]		TEST_DIR,		// always 2'b11 (write)

input		wire	[1:0]		MATCH_B,			// board address matching {A[31:24], A[23:19]}
output	wire				DIR,				// D_DATA direction

input		wire				EXT_DATA,		// NIM input data
output	wire				EXT_OUT,			// NIM output data
output	wire	[1:0]		CLK_OUT,			// NIM output clock, see JP2

output	wire	[1:0]		UCLK,				// Mezzanine Card clock {MC1, MC0}
output	wire	[1:0]		URESET_B,		// Mezzanine Card reset {MC1, MC0}

input		wire	[5:2]		BUF_A,
input		wire				BUF_A10,
input		wire				BUF_A18,
input		wire	[5:3]		BUF_AM,
input		wire				BUF_AM0,
input		wire				BUF_AM1,
input		wire				BUF_AS_B,
input		wire	[1:0]		BUF_DS_B,
input		wire				BUF_IACK_B,
input		wire				BUF_IACKIN_B,
output	wire				BUF_IACKOUT_B,
input		wire				BUF_LWORD_B,
input		wire				BUF_SYSRESET_B,
input		wire				BUF_WRITE_B,
output	wire				CPLD_BERR_B,
output	wire				CPLD_DTACK_B,
output	wire				CPLD_IRQ_B,
inout		wire	[15:0]	D_DATA,

//output	wire				BP_CLK,
//output	wire				BP_RESET_B,

input		wire				FPGA_BUSY,
output	wire				FPGA_CCLK,
input		wire				FPGA_DONE,
input		wire				FPGA_INIT_B,
output	wire				FPGA_PROG_B,
input		wire				FPGA_STATE,
output	wire	[15:8]	CFG_DATA1,
inout		wire	[7:0]		CFG_DATA0,

output	wire				ALL_RESET_B,
output	wire	[1:0]		FPGA_GCLK,
output	wire	[3:0]		FPGA_IA_IN,
input		wire	[3:0]		FPGA_IA_OUT,
output	wire				FPGA_RSTR_B,
output	wire				FPGA_SELECT,
output	wire				FPGA_WSTR_B,

input		wire				DPM_BUSYR_B,
output	wire				DPM_CE0R_B,
output	wire				DPM_OER_B,
output	wire				DPM_RWR

);


//-- Board Select ------------------------------------------------------------------------------------------------	
reg				board_trig	= 1'b0;
reg				dpm_trig		= 1'b0;
reg				fpga_trig	= 1'b0;
reg	[3:0]		fpgaia_reg	= 4'hf;
reg				cpld_trig	= 1'b0;
reg	[3:0]		cpldia_reg	= 4'hf;
reg				read_trig	= 1'b0;
wire	[15:0]	vme_din;
reg	[15:0]	vme_dout		= 16'h0;

wire				cpldread;
wire				cpldwrite;
wire				phase1;
wire				phase2;
wire				reset;
wire				cclk_dsb;
wire				trig_int;

reg	[1:0]		as_sync	= 2'b00;

always @(posedge VME_CLK) begin
	as_sync	<= {as_sync[0], !BUF_AS_B};
end

always @(posedge VME_CLK) begin
	if (as_sync==2'b01) begin
		read_trig	<= BUF_WRITE_B;
		if (MATCH_B!=2'b00) begin
			board_trig	<= 1'b0;
			dpm_trig		<= 1'b0;
			fpga_trig	<= 1'b0;
			fpgaia_reg	<= 4'hf;
			cpld_trig	<= 1'b0;
			cpldia_reg	<= 4'hf;
		end
		else begin
			board_trig	<= 1'b1;
			if ((BUF_AM[5:3]==3'b001) && (BUF_AM1 ^ BUF_AM0) && !BUF_LWORD_B && BUF_IACK_B) begin
				if (BUF_A18) begin
					dpm_trig		<= 1'b1;
					fpga_trig	<= 1'b0;
					cpld_trig	<= 1'b0;
				end
				else begin
					dpm_trig		<= 1'b0;
					if (BUF_A10) begin
						fpga_trig	<= 1'b1;
						fpgaia_reg	<= BUF_A[5:2];
						cpld_trig	<= 1'b0;
						cpldia_reg	<= 4'hf;
					end
					else begin
						fpga_trig	<= 1'b0;
						fpgaia_reg	<= 4'hf;
						cpld_trig	<= 1'b1;
						cpldia_reg	<= BUF_A[5:2];
					end
				end
			end
		end
	end
end	

assign	vme_din		= D_DATA[15:0];
assign	cpldwrite	= cpld_trig && !read_trig && phase1;
assign	cpldread		= cpld_trig && read_trig && phase2;
assign	D_DATA		= cpldread? vme_dout: 16'hz;

//-- FPGA --------------------------------------------------------------------------------------------------------------
assign	FPGA_SELECT			= !BUF_AS_B && fpga_trig;
assign 	FPGA_IA_IN[3:0]	= fpgaia_reg;
assign	FPGA_GCLK[0]	= CLK;
assign	FPGA_GCLK[1]	= VME_CLK;

//-- others --------------------------------------------------------------------------------------------------------------
assign	LED		= {FPGA_IA_OUT[3:0], FPGA_STATE};
assign	BUF_IACKOUT_B	= BUF_IACKIN_B;
assign	CPLD_IRQ_B	= 1'b1;
//assign	BP_RESET_B	= 1'bZ;
//assign	BP_CLK		= 1'bZ;
assign	UCLK[0]			= CLK;
assign	UCLK[1]			= CLK;
assign	CLK_OUT[0]		= CLK;
assign	CLK_OUT[1]		= VME_CLK;

//-- Manual Reset Register(MRR) register1 ----------------------------------------------------------------------
reg	cpldrst	= 1'b0;
reg	mcrst		= 1'b0;
reg	fpgarst	= 1'b0;

always @(posedge VME_CLK) begin
	if (cpldwrite && cpldia_reg==4'h1) begin
		cpldrst	<= vme_din[2];	// write 4 to MRR to reset CPLD
		mcrst		<= vme_din[1];	// write 2 to MRR to reset mezzanine
		fpgarst	<= vme_din[0];	// write 1 to MRR to reset FPGA
	end
end

assign	reset				= (cpldrst || !RESET_B || !BUF_SYSRESET_B);		// for CPLD
assign	URESET_B[1:0]	= {2{!(mcrst || !RESET_B || !BUF_SYSRESET_B)}};	// for mezzanine
assign	ALL_RESET_B		= !(fpgarst || !RESET_B || !BUF_SYSRESET_B);		// for FPGA

wire	[15:0]	mrr_data;
assign			mrr_data	= {8'h0, 1'b0, POK4, POK3, POK1, 1'b0, cpldrst, mcrst, fpgarst};

//-- CPLD Creation Date Register (CCD) register2 ----------------------------------------------------------------------
//-- CPLD Creation Year Register (CCY) register3 ----------------------------------------------------------------------

//-- FPGA Configuration Control (FCC) register4 ----------------------------------------------------------------------
reg	prog_b_reg	= 1'b1;

always @(posedge VME_CLK or posedge reset) begin
	if (reset) begin
		prog_b_reg	<= 1'b1;
	end
	else if (cpldwrite && cpldia_reg==4'h4) begin
		prog_b_reg	<= vme_din[2];
	end
end

assign	FPGA_PROG_B	= prog_b_reg;

wire	[15:0]	fcc_data;
assign			fcc_data	= {8'h0, 2'b00, FPGA_BUSY, FPGA_INIT_B, FPGA_DONE, FPGA_PROG_B, 2'b00};

//-- Trigger Monitor Register (TMR) register5 ----------------------------------------------------------------------
reg			trig_out	= 1'b0;
reg	[3:0]	data_int	= 4'h0;

always @(posedge VME_CLK or posedge reset) begin
	if (reset) begin
		trig_out		<= 1'b0;
		data_int		<= 4'h0;
	end
	else if (cpldwrite && cpldia_reg==4'h5) begin
		trig_out		<= vme_din[12];
		data_int		<= vme_din[11:8];	
	end
end

assign	EXT_OUT	= trig_out || trig_int;

wire	[15:0]	tmr_data;
assign			tmr_data	= {EXT_DATA, EXT_OUT, trig_int, trig_out, data_int[3:0], CFG_DATA0[7:0]};

//-- Config Data Mask Register (CDM) register6 ----------------------------------------------------------------------
reg	[15:0]	cfg_mask	= 16'h0;

always @(posedge VME_CLK or posedge reset) begin
	if (reset) begin
		cfg_mask	<= 16'h0;
	end
	else if (cpldwrite && cpldia_reg==4'h6) begin
		cfg_mask	<= vme_din;
	end
end

assign	cclk_dsb	= |(cfg_mask[15:0]);
assign	trig_int	= (cfg_mask[0] && CFG_DATA0[0]);

//-- Config Data Register (CDR) register7 ----------------------------------------------------------------------------
reg	[15:0]	cfg_data	= 16'h0;

always @(posedge VME_CLK or posedge reset) begin
	if (reset) begin
		cfg_data	<= 16'h0;
	end
	else if (cpldwrite && cpldia_reg==4'h7) begin
		cfg_data	<= vme_din;
	end
end

assign	CFG_DATA1[15]		= EXT_DATA;
assign	CFG_DATA1[14]		= cfg_mask[14]?	EXT_OUT:			cfg_data[14];
assign	CFG_DATA1[13]		= cfg_mask[13]?	trig_int:		cfg_data[13];
assign	CFG_DATA1[12]		= cfg_mask[12]?	trig_out:		cfg_data[12];
assign	CFG_DATA1[11]		= cfg_mask[11]?	data_int[3]:	cfg_data[11];
assign	CFG_DATA1[10]		= cfg_mask[10]?	data_int[2]:	cfg_data[10];
assign	CFG_DATA1[9]		= cfg_mask[9]?		data_int[1]:	cfg_data[9];
assign	CFG_DATA1[8]		= cfg_mask[8]?		data_int[0]:	cfg_data[8];
assign	CFG_DATA0[7]		= cfg_mask[7]?		1'bZ:				cfg_data[7];
assign	CFG_DATA0[6]		= cfg_mask[6]?		1'bZ:				cfg_data[6];
assign	CFG_DATA0[5]		= cfg_mask[5]?		1'bZ:				cfg_data[5];
assign	CFG_DATA0[4]		= cfg_mask[4]?		1'bZ:				cfg_data[4];
assign	CFG_DATA0[3]		= cfg_mask[3]?		1'bZ:				cfg_data[3];
assign	CFG_DATA0[2]		= cfg_mask[2]?		1'bZ:				cfg_data[2];
assign	CFG_DATA0[1]		= cfg_mask[1]?		1'bZ:				cfg_data[1];
assign	CFG_DATA0[0]		= cfg_mask[0]?		1'bZ:				cfg_data[0];

//-- select D_DATA ---------------------------------------------------------------------------------
always @(posedge VME_CLK) begin
	if (phase1) begin
		case (cpldia_reg)
			4'h7: vme_dout <= cfg_data;
			4'h6: vme_dout <= cfg_mask;
			4'h5: vme_dout <= tmr_data;
			4'h4: vme_dout <= fcc_data;
			4'h3: vme_dout <= 16'h2012;
			4'h2: vme_dout <= 16'h1115;
			4'h1: vme_dout <= mrr_data;
			default: vme_dout <= 16'h00f7;
		endcase
	end
end

//-- data cycle --------------------------------------------------------------------------------------
parameter	DTACK_DELAY	= 7; //duration between detecting ds low and driving dtack low, should be > 4
reg	[DTACK_DELAY:0]	cycle;

always @(posedge VME_CLK) begin
	cycle	<= {cycle[DTACK_DELAY-1:0], BUF_DS_B==2'b00 && board_trig};
end

assign	CPLD_DTACK_B	= !(cycle[DTACK_DELAY] && cycle[3]);
assign	CPLD_BERR_B		= 1'b1;
assign	DIR		= {!(read_trig && phase2)};
assign	phase1	= (cycle[0] && !cycle[2]);
assign	phase2	= (cycle[0] || cycle[2]);

assign	DPM_RWR		= !(dpm_trig && !read_trig);
assign	DPM_OER_B	= !((cycle[1] && !cycle[DTACK_DELAY]) && read_trig && dpm_trig);
assign	DPM_CE0R_B	= DPM_RWR? DPM_OER_B: !(cycle[0] && !cycle[1] && dpm_trig);

assign	FPGA_CCLK	= cclk_dsb? 1'b0: (cycle[5] && cycle[0] && cpld_trig && !read_trig && cpldia_reg==4'h7);
assign 	FPGA_WSTR_B	= !(!read_trig && cycle[0] && FPGA_SELECT);
assign 	FPGA_RSTR_B	= !(read_trig && cycle[0] && FPGA_SELECT);

//-- test pin ---------------------------------------------------------------------------------------------------------
assign	CPLD_TEST	= {RESET_B,
								DIR,
								MATCH_B[1],
								MATCH_B[0],
								DPM_BUSYR_B,
								DPM_RWR,
								DPM_OER_B,
								DPM_CE0R_B,
								URESET_B[1],
								URESET_B[0],
								ALL_RESET_B,
								FPGA_CCLK,
								FPGA_BUSY,
								FPGA_INIT_B,
								FPGA_DONE,
								FPGA_PROG_B};
assign	TEST_DIR	= 2'b11;

endmodule
