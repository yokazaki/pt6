`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: Kyoto University
// Engineer: Takuto KUNIGO
// 
// Create Date:    8/12/2015
// Design Name: 
// Module Name:    glink TX TX 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////


 module glTXTX
(
// TX 1
output	wire	[15:0]	TX_1,
output	wire				TXFLAG_1,
output	wire				TXDATA_1,
output	wire				TXCNTL_1,
output	wire	[1:0]		TXDIV_1,
output	wire				TXFLGENB_1,
output	wire				TXESMPXENB_1,
output	wire				TXTCLKENB_1,
output	wire				TXDIS_1,
input		wire				TXLOCKED_1,


// TX 2
output	wire	[15:0]	TX_2,
output	wire				TXFLAG_2,
output	wire				TXDATA_2,
output	wire				TXCNTL_2,
output	wire	[1:0]		TXDIV_2,
output	wire				TXFLGENB_2,
output	wire				TXESMPXENB_2,
output	wire				TXTCLKENB_2,
output	wire				TXDIS_2,
input		wire				TXLOCKED_2,




// VME DATA
//inout		wire	[15:0]	D_DATA,
// VME ADDRESS
//input		wire	[7:0]		FPGA_IA_IN,

input		wire				EXT_DATA,
input		wire				FPGA_GCLK,
input		wire				ALL_RESET_B,
output	wire				FPGA_STATE,
input		wire				FPGA_SELECT,
input		wire				FPGA_WSTR_B,
input		wire				FPGA_RSTR_B
); 

assign FPGA_STATE = TXLOCKED_1 && TXLOCKED_2;

wire	[8:0]	glink_status_1, glink_status_2; 
// { enable | dis, txtclkenb, esmpxenb, flagenb | mode, div }

reg	enable_extd = 1'b0;
always @(posedge EXT_DATA)
	begin
		enable_extd <= !enable_extd;
	end

wire enable_1, enable_2;
assign enable_1 = enable_extd | glink_status_1[8];
assign enable_2 = enable_extd | glink_status_2[8];

/*wire	[15:0]	tx_data_1[0:15];
wire	[15:0]	tx_data_2[0:15];
wire				tx_flag_1[0:15];
wire				tx_flag_2[0:15];
*/
reg glinktx_flg = 1'b0;
reg [15:0] count_data;

assign TX_1[15:0] = {count_data[3:0], count_data[15:4]};
assign TX_2[15:0] = {count_data[3:0], count_data[15:4]};
assign TXFLAG_1 = 1'b0;
assign TXFLAG_2 = 1'b0;
assign TXDATA_1 = 1'b1;
assign TXDATA_2 = 1'b1;
assign TXCNTL_1 = 1'b0;
assign TXCNTL_2 = 1'b0;
assign TXDIV_1[1:0] = 2'b01;
assign TXDIV_2[1:0] = 2'b01;
assign TXFLGENB_1 = 1'b1;
assign TXFLGENB_2 = 1'b1;
assign TXESMPXENB_1 = 1'b0;
assign TXESMPXENB_2 = 1'b0;
assign TXTCLKENB_1 = 1'b0;
assign TXTCLKENB_2 = 1'b0;
assign TXDIS_1 = 1'b0;
assign TXDIS_2 = 1'b0;


always @(posedge FPGA_GCLK) begin
		count_data[15:0] <= count_data[15:0] + 16'h1;
end


/*
glinkTX TX1(.CLK(FPGA_GCLK), .RESET(ALL_RESET_B),
				.in_data0(tx_data_1[0]),  .in_data1(tx_data_1[1]),  .in_data2(tx_data_1[2]),  .in_data3(tx_data_1[3]),
				.in_data4(tx_data_1[4]),  .in_data5(tx_data_1[5]),  .in_data6(tx_data_1[6]),  .in_data7(tx_data_1[7]),
				.in_data8(tx_data_1[8]),  .in_data9(tx_data_1[9]),  .in_dataa(tx_data_1[10]), .in_datab(tx_data_1[11]),
				.in_datac(tx_data_1[12]), .in_datad(tx_data_1[13]), .in_datae(tx_data_1[14]), .in_dataf(tx_data_1[15]),
				.in_flag0(tx_flag_1[0]),  .in_flag1(tx_flag_1[1]),  .in_flag2(tx_flag_1[2]),  .in_flag3(tx_flag_1[3]),
				.in_flag4(tx_flag_1[4]),  .in_flag5(tx_flag_1[5]),  .in_flag6(tx_flag_1[6]),  .in_flag7(tx_flag_1[7]),
				.in_flag8(tx_flag_1[8]),  .in_flag9(tx_flag_1[9]),  .in_flaga(tx_flag_1[10]), .in_flagb(tx_flag_1[11]),
				.in_flagc(tx_flag_1[12]), .in_flagd(tx_flag_1[13]), .in_flage(tx_flag_1[14]), .in_flagf(tx_flag_1[15]),				
				.in_mode(glink_status_1[3:2]), .in_locked(TXLOCKED_1), .in_enabled(enable_1),
				.in_div(glink_status_1[1:0]), .in_esmpxenb(glink_status_1[5]), .in_flgenb(glink_status_1[4]), .in_tclkenb(glink_status_1[6]), .in_dis(glink_status_1[7]),
				.out_data(TX_1), .out_flag(TXFLAG_1), .out_mode({TXCNTL_1, TXDATA_1}),
				.out_div(TXDIV_1), .out_flgenb(TXFLGENB_1), .out_esmpxenb(TXESMPXENB_1),
				.out_tclkenb(TXTCLKENB_1), .out_dis(TXDIS_1)
				);	 

glinkTX TX2(.CLK(FPGA_GCLK), .RESET(ALL_RESET_B),
				.in_data0(tx_data_2[0]),  .in_data1(tx_data_2[1]),  .in_data2(tx_data_2[2]),  .in_data3(tx_data_2[3]),
				.in_data4(tx_data_2[4]),  .in_data5(tx_data_2[5]),  .in_data6(tx_data_2[6]),  .in_data7(tx_data_2[7]),
				.in_data8(tx_data_2[8]),  .in_data9(tx_data_2[9]),  .in_dataa(tx_data_2[10]), .in_datab(tx_data_2[11]),
				.in_datac(tx_data_2[12]), .in_datad(tx_data_2[13]), .in_datae(tx_data_2[14]), .in_dataf(tx_data_2[15]),
				.in_flag0(tx_flag_2[0]),  .in_flag1(tx_flag_2[1]),  .in_flag2(tx_flag_2[2]),  .in_flag3(tx_flag_2[3]),
				.in_flag4(tx_flag_2[4]),  .in_flag5(tx_flag_2[5]),  .in_flag6(tx_flag_2[6]),  .in_flag7(tx_flag_2[7]),
				.in_flag8(tx_flag_2[8]),  .in_flag9(tx_flag_2[9]),  .in_flaga(tx_flag_2[10]), .in_flagb(tx_flag_2[11]),
				.in_flagc(tx_flag_2[12]), .in_flagd(tx_flag_2[13]), .in_flage(tx_flag_2[14]), .in_flagf(tx_flag_2[15]),				
				.in_mode(glink_status_2[3:2]), .in_locked(TXLOCKED_2), .in_enabled(enable_2),
				.in_div(glink_status_2[1:0]), .in_esmpxenb(glink_status_2[5]), .in_flgenb(glink_status_2[4]), .in_tclkenb(glink_status_2[6]), .in_dis(glink_status_2[7]),
				.out_data(TX_2), .out_flag(TXFLAG_2), .out_mode({TXCNTL_2, TXDATA_2}),
				.out_div(TXDIV_2), .out_flgenb(TXFLGENB_2), .out_esmpxenb(TXESMPXENB_2),
				.out_tclkenb(TXTCLKENB_2), .out_dis(TXDIS_2)
				);	 
*/
// VME Read/Write
/*VMEEncoder VMEEncoder(
	.RESET_B(ALL_RESET_B), .CLK(FPGA_GCLK),
	.CE_B(FPGA_SELECT), .OE_B(FPGA_RSTR_B), .WE_B(FPGA_WSTR_B),
	.VME_A(FPGA_IA_IN), .VME_D(D_DATA),
	.glink_status_1(glink_status_1), .glink_status_2(glink_status_2),
	.glink_data_10(tx_data_1[0]),  .glink_data_11(tx_data_1[1]),  .glink_data_12(tx_data_1[2]),  .glink_data_13(tx_data_1[3]),
	.glink_data_14(tx_data_1[4]),  .glink_data_15(tx_data_1[5]),  .glink_data_16(tx_data_1[6]),  .glink_data_17(tx_data_1[7]),
	.glink_data_18(tx_data_1[8]),  .glink_data_19(tx_data_1[9]),  .glink_data_1a(tx_data_1[10]), .glink_data_1b(tx_data_1[11]),
	.glink_data_1c(tx_data_1[12]), .glink_data_1d(tx_data_1[13]), .glink_data_1e(tx_data_1[14]), .glink_data_1f(tx_data_1[15]),
	.glink_data_20(tx_data_2[0]),  .glink_data_21(tx_data_2[1]),  .glink_data_22(tx_data_2[2]),  .glink_data_23(tx_data_2[3]),
	.glink_data_24(tx_data_2[4]),  .glink_data_25(tx_data_2[5]),  .glink_data_26(tx_data_2[6]),  .glink_data_27(tx_data_2[7]),
	.glink_data_28(tx_data_2[8]),  .glink_data_29(tx_data_2[9]),  .glink_data_2a(tx_data_2[10]), .glink_data_2b(tx_data_2[11]),
	.glink_data_2c(tx_data_2[12]), .glink_data_2d(tx_data_2[13]), .glink_data_2e(tx_data_2[14]), .glink_data_2f(tx_data_2[15]),
	.glink_flag_10(tx_flag_1[0]),  .glink_flag_11(tx_flag_1[1]),  .glink_flag_12(tx_flag_1[2]),  .glink_flag_13(tx_flag_1[3]),
	.glink_flag_14(tx_flag_1[4]),  .glink_flag_15(tx_flag_1[5]),  .glink_flag_16(tx_flag_1[6]),  .glink_flag_17(tx_flag_1[7]),
	.glink_flag_18(tx_flag_1[8]),  .glink_flag_19(tx_flag_1[9]),  .glink_flag_1a(tx_flag_1[10]), .glink_flag_1b(tx_flag_1[11]),
	.glink_flag_1c(tx_flag_1[12]), .glink_flag_1d(tx_flag_1[13]), .glink_flag_1e(tx_flag_1[14]), .glink_flag_1f(tx_flag_1[15]),
	.glink_flag_20(tx_flag_2[0]),  .glink_flag_21(tx_flag_2[1]),  .glink_flag_22(tx_flag_2[2]),  .glink_flag_23(tx_flag_2[3]),
	.glink_flag_24(tx_flag_2[4]),  .glink_flag_25(tx_flag_2[5]),  .glink_flag_26(tx_flag_2[6]),  .glink_flag_27(tx_flag_2[7]),
	.glink_flag_28(tx_flag_2[8]),  .glink_flag_29(tx_flag_2[9]),  .glink_flag_2a(tx_flag_2[10]), .glink_flag_2b(tx_flag_2[11]),
	.glink_flag_2c(tx_flag_2[12]), .glink_flag_2d(tx_flag_2[13]), .glink_flag_2e(tx_flag_2[14]), .glink_flag_2f(tx_flag_2[15])
	);
*/
endmodule