// Pt6Module.cc

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <iomanip>
#include <stdint.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "Pt6Module.hh"

#define DEVICE "/dev/vmedrv32d32"
#define SIZE 0x80000 //20b1000_0000_0000_0000_0000: 65536*8= 524288

using namespace std;

Pt6Module::Pt6Module( off_t vmeaddr )
{
	fd = open( DEVICE, O_RDWR ); //O_RDWR means "open for R/W" 
	if ( fd == -1 ) {
		cerr << "ERROR: cannot open " << DEVICE << endl;
		exit(1);
	}
	
	base_address =
	static_cast<uint32_t*>( mmap( 0, SIZE, PROT_READ | PROT_WRITE,
                                       MAP_SHARED, fd, vmeaddr ) );
	if ( base_address == (uint32_t*)-1 ) {
		cerr << "ERROR: cannot mmap" << endl;
		exit(1);
	}
}

Pt6Module::~Pt6Module()
{
	munmap( base_address, SIZE );
	close( fd );
}

Pt6Module::result Pt6Module::writevme( chip_id chip, uint32_t address,
                                       uint32_t value )
{
	if ( (chip != DPM) & address > 255 )
	return INVALID_ADDRESS;
	
	if ( (chip == DPM) & address > 65535 )
	return INVALID_ADDRESS;
	
	switch ( chip ) {
		case CPLD:
		base_address[ 0 + address] = value;
		break;
		
		case FPGA:
		base_address[256 + address] = value;
		break;
		
		case DPM:
		base_address[65536 + address] = value;
		break;
		
		default:
		return INVALID_CHIP_ID;
    }
	return NO_ERROR;
}

Pt6Module::result Pt6Module::readvme( chip_id chip, uint32_t address,
                                      uint32_t * value_p )
{
	if ( (chip != DPM) & address > 255 )
	return INVALID_ADDRESS;
	
	if ( (chip == DPM) & address > 65535 )
	return INVALID_ADDRESS;
	
	switch ( chip ) {
		case CPLD:
		*value_p = base_address[ 0 + address];
		break;
		
		case FPGA:
		*value_p = base_address[256 + address];
		break;
		
		case DPM:
		*value_p = base_address[65536 + address];
		break;
		
		default:
		return INVALID_CHIP_ID;
	}
	return NO_ERROR;
}

Pt6Module::result Pt6Module::dump( const char * filename )
{
	ofstream datfile( filename );
	uint32_t data = 0;
	uint32_t count = 0;

	if ( !datfile )
	return CANNOT_OPEN_FILE;
	
	while ( count < 0x0000ffff ){
		readvme( DPM, count, &data );

		data = data & 0x0000ffff;

		datfile << "addr=0x" << setfill( '0' ) << setw( 4 ) << hex << count
			<< "\tdata=0x" << setfill ( '0' ) << setw( 4 ) << hex << data << "\n";

		if ( count  == 0x0000fffe )
			std::cout << "file=" << filename << std::endl;

		count = count + 1;
	}
	
	return NO_ERROR;
}

Pt6Module::result Pt6Module::sdump( const char * filename )
{
	ofstream datfile( filename );
	uint32_t data = 0;
	uint32_t pre_data = 0;
	uint32_t count = 0;
	uint32_t slip = 0xffffffff;

	if ( !datfile )
	return CANNOT_OPEN_FILE;
	
	while ( count < 0x0000ffff ){
		readvme( DPM, count, &data );

		data = data & 0x0000ffff;

		if ( data != pre_data + 1 & data !=0x00000000){
			slip = slip + 1;
			datfile << "addr=0x" << setfill( '0' ) << setw( 4 ) << hex << count
				<< "\tdata=0x" << setfill ( '0' ) << setw( 4 ) << hex << data
				<< "\tslip=" << dec << slip << "\n";
		}

		if ( count  == 0x0000fffe )
			std::cout << "file=" << filename << "\tslip=" <<  slip << std::endl;

		pre_data = data;
		count = count + 1;
	}
	
	return NO_ERROR;
}
/*
Pt6Module::result Pt6Module::blank()
{
	uint32_t data = 0;
	uint32_t count = 0;

	while ( count < 65535 ){
		writevme( DPM, count, data );

		count = count + 1;
	}
	
	return NO_ERROR;
}
*/
Pt6Module::result Pt6Module::erase()
{
	uint32_t check;
	int32_t count = 0;
	
	writevme( CPLD, 4, 0 );
	count = 0;
	while( true ) {
		readvme( CPLD, 4, &check );
		if( !( check & 16UL ) )
		break;
		count++;
		if( count == 2000 ) {
			std::cout << "init high timeout" << std::endl;
			return ERASE_FAIL;
		}
	}
	
	writevme( CPLD, 4, 4 );
	count = 0;
	while( true ) {
		readvme( CPLD, 4, &check );
		if( ( check & 16UL ) )
		break;
		count++;
		if( count == 2000 ) {
			std::cout << "init low timeout" << std::endl;
			return ERASE_FAIL;
		}
	}
	readvme( CPLD, 4, &check );
	if ( check & 8UL )
	return ERASE_FAIL;
	
	else
	return NO_ERROR;
}

Pt6Module::result Pt6Module::configure( const char * filename )
{
	ifstream bitfile( filename );
	char data1, data0;
	uint32_t cfgdata;
	uint32_t check;
	if ( !bitfile )
	return CANNOT_OPEN_FILE;
	
	char check_data[8] = {0,0,0,0,0,0,0,0};
	int32_t count = 0;
	while ( !( ( check_data[7] == (char)0xff )
			& ( check_data[6] == (char)0xff )
			& ( check_data[5] == (char)0xff )
			& ( check_data[4] == (char)0xff )
			& ( check_data[3] == (char)0xaa )
			& ( check_data[2] == (char)0x99 )
			& ( check_data[1] == (char)0x55 )
			& ( check_data[0] == (char)0x66 ) ) )
	{
		check_data[7] = check_data[6];
		check_data[6] = check_data[5];
		check_data[5] = check_data[4];
		check_data[4] = check_data[3];
		check_data[3] = check_data[2];
		check_data[2] = check_data[1];
		check_data[1] = check_data[0];
		check_data[0] = bitfile.get();
		++count;
		if ( count > 200 )
		return INVALID_FILE;
	}	
	bitfile.seekg( -20, ios::cur );
	writevme( CPLD, 4, 0 );
	count = 0;
	while( true ) {
		readvme( CPLD, 4, &check );
		if( !( check & 16UL ) )
		break;
		count++;
		if( count == 2000 ) {
			std::cout << "init high timeout" << std::endl;
			return CONF_FAIL;
		}
	}
	
	writevme( CPLD, 4, 4 );
	count = 0;
	while( true ) {
		readvme( CPLD, 4, &check );
		if( ( check & 16UL ) )
		break;
		count++;
		if( count == 2000 ) {
			std::cout << "init low timeout" << std::endl;
			return CONF_FAIL;
		}
	}
//	ofstream tmp( "writtendata.bit" );
	
	count = 0;
	while( true ) {
		data1 = bitfile.get();
		data0 = bitfile.get();
		if( bitfile.eof() )
		break;
		
		cfgdata =
		( ( data1 &   1UL ) << 15 ) |
		( ( data1 &   2UL ) << 13 ) |
		( ( data1 &   4UL ) << 11 ) |
		( ( data1 &   8UL ) <<  9 ) |
		( ( data1 &  16UL ) <<  7 ) |
		( ( data1 &  32UL ) <<  5 ) |
		( ( data1 &  64UL ) <<  3 ) |
		( ( data1 & 128UL ) <<  1 ) |
		( ( data0 &   1UL ) <<  7 ) |
		( ( data0 &   2UL ) <<  5 ) |
		( ( data0 &   4UL ) <<  3 ) |
		( ( data0 &   8UL ) <<  1 ) |
		( ( data0 &  16UL ) >>  1 ) |
		( ( data0 &  32UL ) >>  3 ) |
		( ( data0 &  64UL ) >>  5 ) |
		( ( data0 & 128UL ) >>  7 );
//		tmp << cfgdata;
		writevme( CPLD, 7, cfgdata );
//		readvme( CPLD, 4, &check );
//		std::cout << "busy check = " << std::hex << check <<std::endl;
//		if( check & 32UL ) return CONF_FAIL;
//		count++;
//		if( count % 100000 == 0)
//		std::cout << "count = " << std::dec << count << "/2110106" <<std::endl;
	}
	readvme( CPLD, 4, &check );
	std::cout << "done check = "<< std::hex << check << std::endl;
	if ( check & 8UL )
	return NO_ERROR;
	
	else
	return CONF_FAIL;
}
